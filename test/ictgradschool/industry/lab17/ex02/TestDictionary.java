package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * TODO Implement this class.
 */
public class TestDictionary {

    Dictionary newDictionary = new Dictionary();

    @Test
   public void testDicitionary(){

        assertTrue(newDictionary.isSpellingCorrect("this"));
        assertTrue(newDictionary.isSpellingCorrect("the"));
        assertFalse(newDictionary.isSpellingCorrect("123"));
    }
}