package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;
import org.omg.CORBA.PUBLIC_MEMBER;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * TODO Implement this class.
 */
public class TestSimpleSpellChecker {

    SimpleSpellChecker newSimpleChecker;

    @Before
    public void setup(){
        try {
           newSimpleChecker = new SimpleSpellChecker(new Dictionary(),"Hello worl1d 123 wordl word w 12word");
        }catch (InvalidDataFormatException e){
            e.getMessage();
        }
    }


    @Test
    public void testGetUniqueWords(){
        assertEquals(Arrays.asList("123", "wordl", "worl1d", "w", "hello", "word", "12word"),newSimpleChecker.getUniqueWords());

    }


    @Test
   public void testGetMisspelledWords(){
        assertEquals(Arrays.asList("123", "wordl", "worl1d", "w", "12word"),newSimpleChecker.getMisspelledWords());
    }

    @Test
    public void testGetFrequencyOfWord(){
       try {
           assertEquals(1,newSimpleChecker.getFrequencyOfWord("Hello"));
       }catch (InvalidDataFormatException e){
           e.getMessage();
       }

    }
}