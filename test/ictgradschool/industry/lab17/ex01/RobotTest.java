package ictgradschool.industry.lab17.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    @Test
    public void testingBacktrack(){
        for (int i = 0; i <5 ; i++) {
            myRobot.turn();
        }
        int size = myRobot.states.size();
        myRobot.backTrack();
        assertEquals(myRobot.states.size(),size-1 );
    }

    @Test
    public void testingCurrentState(){
        Robot.RobotState size = myRobot.states.get( myRobot.states.size()-1);
        assertEquals(myRobot.states.get(myRobot.states.size()-1), size );
    }

    @Test
    public void testTurn(){
        Robot.RobotState currentState = myRobot.currentState();
        myRobot.turn();
        assertEquals(currentState.column, myRobot.column());
        assertEquals(currentState.row, myRobot.row());
        switch (myRobot.getDirection()){

            case North:
                assertEquals(Robot.Direction.East, myRobot.getDirection());
                break;
           /* case East:
                assertEquals(Robot.Direction.South, myRobot.getDirection());
                break;*/
            case South:
                assertEquals(Robot.Direction.West, myRobot.getDirection());
                break;
            case West:
                assertEquals(Robot.Direction.North, myRobot.getDirection());
                break;
        }
    }




    @Test
    public void testMoveNorth() throws IllegalMoveException{
        if (myRobot.currentState().direction == Robot.Direction.North){
            int originalRow = myRobot.currentState().row;
            myRobot.move();
            assertTrue(myRobot.currentState().row == originalRow - 1);
        }
    }

    @Test
    public void testMoveSouth() throws IllegalMoveException{
        if (myRobot.currentState().direction == Robot.Direction.South){
            int originalRow = myRobot.currentState().row;
            myRobot.move();
            assertTrue(myRobot.currentState().row == originalRow +1);
        }
    }

    @Test
    public void testMoveEast() throws IllegalMoveException{
        if (myRobot.currentState().direction == Robot.Direction.East){
            int originalCol = myRobot.currentState().column;
            myRobot.move();
            assertTrue(myRobot.currentState().column== originalCol + 1);
        }
    }


    @Test
    public void testMoveWest() throws IllegalMoveException{
        if (myRobot.currentState().direction == Robot.Direction.West){
            int originalCol = myRobot.currentState().column;
            myRobot.move();
            assertTrue(myRobot.currentState().column== originalCol - 1);
        }
    }

    @Test
    public void testIllegalMoveSouth() {
        myRobot.turn();
        myRobot.turn(); // turn the robot facing south
        try {
            // Now try to move South.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().row);
        }
    }

    @Test
    public void testIllegalMoveWest() {
        myRobot.turn();
        myRobot.turn();
        myRobot.turn(); // turn the robot facing West
        try {
            // Now try to move west.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().column);
        }
    }

    @Test
    public void testIllegalMoveEast() {
        boolean atRight = false;
        try {
            myRobot.turn();// turn to face east
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++) {
                myRobot.move();
            }

            // Check that robot has reached the top.
            atRight = myRobot.currentState().column == 10;
            assertTrue(atRight);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().column);
        }
    }









}
