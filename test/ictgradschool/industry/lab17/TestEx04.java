package ictgradschool.industry.lab17;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;

/**
 * Created by lelezhao on 5/21/17.
 */
public class TestEx04 {
    private ex04 testTool = new ex04();

    @Test
    public void TestConvertUserInputtoDouble() throws NullPointerException, NumberFormatException {







    }

    @Test
    public void testGetAreaofCircle(){
        assertEquals(1256.63,testTool.getAreaofCircle(20),0.01);
        assertEquals(706.85,testTool.getAreaofCircle(15),0.01);
    }

    @Test
    public void testGetAreaofRectangle(){
        assertEquals(286.75,testTool.getAreaofRectangle(15.5,18.5));
    }

    @Test
    public void testGetRoundNumber(){
        assertEquals(287,testTool.getRoundNumber(286.75));
    }


}
