package ictgradschool.industry.lab17;

import ictgradschool.Keyboard;
import ictgradschool.industry.lab17.ex02.InvalidDataFormatException;

/**
 * Created by lelezhao on 5/21/17.
 */
public class ex04 {

    private String userWidth;
    private String userLength;
    private double width,length;

    public void start(){

        while (true) {
            try {
                welcomeMessage();
                System.out.print("Enter the width of the rectangle: ");
                width = convertUserInputtoDouble(Keyboard.readInput());
                System.out.print("Enter the length of the rectangle: ");
                length = convertUserInputtoDouble(Keyboard.readInput());
                break;

            } catch (NullPointerException e) {

                System.out.println("Please enter a number!");


            } catch (NumberFormatException e) {

                System.out.println("Please enter a valid number! \n");


            }
        }

        System.out.println();
        System.out.println("The radius of the circle is: 20");

        int recArea = getRoundNumber(getAreaofRectangle(width,length));
        int cirArea = getRoundNumber(getAreaofCircle(20));


        System.out.println("The smaller area is: " + Math.min(recArea,cirArea));


    }


    public void welcomeMessage(){
        System.out.println("Welcome to Shape Area Calculator!");
        System.out.println();

    }

    public double convertUserInputtoDouble(String input) throws NullPointerException,NumberFormatException{

        return Double.parseDouble(input);
    }

    public double getAreaofRectangle(double recWidth,double recLength){
        return recWidth * recLength;

    }

    public double getAreaofCircle(int radius){

        return Math.PI * Math.pow(radius, 2);

    }

    public int getRoundNumber(double numbers) {
        return (int) Math.round(numbers);
    }



    public static void main(String[] args) {

        new ex04().start();


    }






}
